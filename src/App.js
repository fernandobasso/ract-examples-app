import React from 'react';
import {
  BrowserRouter as Router,
} from 'react-router-dom';

import Nav from './components/Nav';
import Pages from './pages/Pages';

import './App.css';

const App = () => {
  return (
    <Router>
      <div className="App">
        <header className="App-header">
          <Nav />
        </header>

        <Pages />

      </div>
    </Router>
  );
};

export default App;

