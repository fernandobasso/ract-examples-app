import React from 'react';
import { render } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import Nav from '../Nav';

const wrapWithMemoryRouter = (Comp) => {
  return (
    <MemoryRouter>
      <Comp />
    </MemoryRouter>
  );
};

const testHost = 'http://localhost';

describe('Nav.js', () => {
  it('should render', () => {
    const { getByText } = render(wrapWithMemoryRouter(Nav));
    expect(getByText('React Examples')).toBeInTheDocument();
  });

  it('should have the home / link', () => {
    const { getByText } = render(wrapWithMemoryRouter(Nav));
    expect(getByText('React Examples').href).toStrictEqual(`${testHost}/`);
  });

  it('should have the /about link', () => {
    const { getByText } = render(wrapWithMemoryRouter(Nav));
    expect(getByText('About').href).toStrictEqual(`${testHost}/about`);
  });
});

