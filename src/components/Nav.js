import React from 'react';

import {
  Link,
} from 'react-router-dom';

const Nav = () => {
  return (
    <nav className="nav-main">
      <ul>
        <li>
          <Link to="/">React Examples</Link>
        </li>
        <li>
          <Link to="/about">About</Link>
        </li>
      </ul>
    </nav>
  );
};

export default Nav;

