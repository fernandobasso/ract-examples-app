import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

describe('App.js', () => {
  it('renders learn react link', () => {
    const { container } = render(<App />);
    expect(container.querySelector('nav.nav-main')).toBeInTheDocument();
  });
});

