import React from 'react';
import {
  Switch,
  Route,
} from 'react-router-dom';

import PageHome from './PageHome';
import PageAbout from './PageAbout';
import PageGithubFollowers from './PageGithubFollowers';

const Pages = () => {
  return (
    <Switch>
      <Route path="/github-followers">
        <PageGithubFollowers />
      </Route>
      <Route path="/about">
        <PageAbout />
      </Route>
      <Route path="/">
        <PageHome />
      </Route>
    </Switch>
  );
};

export default Pages;

