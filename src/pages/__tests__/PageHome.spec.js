import React from 'react';
import { render } from '@testing-library/react';

import PageHome from '../PageHome';

describe('PageHome.js', () => {
  it('should have the correct h2 text', () => {
    const { getByText } = render(<PageHome />);
    expect(getByText('Home Page')).toBeInTheDocument();
  });
});

