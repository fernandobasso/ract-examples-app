# React Examples App

## Steps to run and develop the app

First time:

```
$ git clone git@gitlab.com:fernandobasso/ract-examples-app.git
$ cd react-examples-app
$ npm install
```

Run locally:

```
$ npm run start
```

To run tests:

```
$ npm run test
```

```
$ npm run lint
```

