/**
 * @var {boolean}
 */
const isDev = process.env.NODE_ENV === 'development';

/**
 * @var {boolean}
 */
const isTest = process.env.NODE_ENV === 'test';

/**
 * @var {boolean}
 */
const isPrd = process.env.NODE_ENV === 'production';


module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true,
    'jest/globals': true,
  },
  extends: [
    'plugin:react/recommended',
    'standard',
    // 'react-app',
    'plugin:jest/all',
  ],
  settings: {
    react: {
      version: 'detect',
    },
  },
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: [
    'react',
    'jest',
  ],
  rules: {
    'semi': [2, 'always'],
    'no-console': isDev ? 'off' : 'error',
    'no-debugger': isDev || isTest ? 'off' : 'error',
    'no-unused-vars': isDev ? 'warn' : 'error',
    'comma-dangle': [ isDev || isTest ? 'warn' : 'error', 'always-multiline' ],
    'no-multiple-empty-lines': ['error', { max: 2, maxBOF: 1, maxEOF: 1 }],

    //
    // Some files will have other exports later and then will have
    // to find and change imports across the project. Let's disable this
    //
    'import/prefer-default-export': 'off',

    //
    // It is okay to have code that just returns a value, but still uses a
    // normal function body:
    //
    //    const fn = (val) => {
    //      return val.toString();
    //    };
    //
    // We don't need to make it mandatory to write it as:
    //
    //    const fn = (val) => val.toString();
    //
    'arrow-body-style': [0, 'never'],

    //
    // Fuck this no-bitwise “oh I'm so dumb” rule.
    //
    // “We are hackers, and hackers use black terminals with green colors.”
    //
    'no-bitwise': 'off',


    ////////////////////////////////////////////////////////////////////////////
    // JEST RULES INI
    //
    // Above, we enable jest globals and apply all rules (not just the
    // recommended ones. Below we do some configs specific for this project.
    //

    //
    // We need to “UpperCase” in ‘describe’ when we are dealing
    // with a component. Ex:
    //
    //   describe('UserProfile.js', () => { ... });
    //
    'jest/lowercase-name': [
      'error',
      { ignore: ['describe'] },
    ],

    //
    // Thank you, but we know we write tests so we can write assertions.
    // We don't need repetitive and boilerplate code inside each and every
    // ‘it’ block.
    //
    'jest/prefer-expect-assertions': 'off',

    //
    // JEST RULES END
    ////////////////////////////////////////////////////////////////////////////
  },
  ignorePatterns: [
    'src/serviceWorker.js',
  ],
};

